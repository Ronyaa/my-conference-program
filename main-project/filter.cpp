﻿#include "filter.h"
#include <cstring>
#include <iostream>

conference_program** filter(conference_program* array[], int size, bool (*check)(conference_program* element), int& result_size)
{
	conference_program** result = new conference_program * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_conference_program_by_speaker(conference_program* element)
{
	return strcmp(element->speaker.first_name, "") == 0 &&
		strcmp(element->speaker.middle_name, "") == 0 &&
		strcmp(element->speaker.last_name, "") == 0;
}

bool check_conference_program_by_date(conference_program* element)
{
	double dif;
	dif = fabs(element->finish.minutes - element->start.minutes);
	return dif > 15;
}
