#pragma once
#ifndef FILTER_H
#define FILTER_H

#include "conference_program.h"

conference_program** filter(conference_program* array[], int size, bool (*check)(conference_program* element), int& result_size);

/*
  <function_name>:
              ,
          true,
    ,

:
    array       -
    size        -
    check       -    .

                   ,
    result_data - ,    - ,



          ,
     (     true)
*/


bool check_conference_program_by_speaker(conference_program* element);

/*
  check_conference_program_by_speaker:
      - ,

:
    element -   ,


    true,           ,  false
*/


bool check_conference_program_by_date(conference_program* element);

/*
  check_conference_program_by_date:
      - ,           2021-

:
    element -   ,


    true,           2021- ,  false
*/

#endif
