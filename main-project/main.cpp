#include <iostream>
#include <iomanip>

using namespace std;

#include "conference_program.h"
#include "file_reader.h"
#include "constants.h"

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �2. ��������� �����������\n";
    cout << "�����: �������� ����������\n\n";
    cout << "������: 16\n";
    conference_program* conference[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", conference, size);
        cout << "***** ��������� ����������� *****\n\n";
        for (int i = 0; i < size; i++)
        {
            /********** ����� ����������� **********/
            cout << "�����������...........: ";
            // ����� ������� �������
            cout << conference[i]->speaker.last_name << " ";
            // ����� ������ ����� ����� �������
            cout << conference[i]->speaker.first_name[0] << ". ";
            // ����� ������ ����� �������� �������
            cout << conference[i]->speaker.middle_name[0] << ".";
            cout << ", ";
            // ����� ����
            cout << '"' << conference[i]->topic << '"';
            cout << '\n';
            /********** ����� ������� ������ **********/
            // ����� �����
            cout << "����� ������.....: ";
            cout << setw(2) << setfill('0') << conference[i]->start.hours << '-';
            // ����� �����
            cout << setw(2) << setfill('0') << conference[i]->start.minutes << '-';
            cout << '\n';
            /********** ����� ������� ����� **********/
            // ����� �����
            cout << "����� �����...: ";
            cout << setw(2) << setfill('0') << conference[i]->finish.hours << '-';
            // ����� �����
            cout << setw(2) << setfill('0') << conference[i]->finish.minutes << '-';
            cout << '\n';
            cout << '\n';
        }
        for (int i = 0; i < size; i++)
        {
            delete conference[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}