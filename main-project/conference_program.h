#pragma once
#ifndef CONFERENCE_PROGRAM_H
#define CONFERENCE_PROGRAM_H

#include "constants.h"

struct date
{
    int hours;
    int minutes;
};

struct person
{
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
    char last_name[MAX_STRING_SIZE];
};

struct conference_program
{
    date start;
    date finish;
    person speaker;
    char topic[MAX_STRING_SIZE];
};

#endif